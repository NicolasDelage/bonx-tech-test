FROM gitpod/workspace-full:2023-05-08-21-16-55

RUN sudo install-packages xdg-utils

RUN bash -c 'VERSION="18.14.2" \
    && source $HOME/.nvm/nvm.sh && nvm install $VERSION \
    && nvm use $VERSION && nvm alias default $VERSION'

RUN echo "nvm use default &>/dev/null" >> ~/.bashrc.d/51-nvm-fix \
    && npm install -g turbo