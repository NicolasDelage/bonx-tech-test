import type { FastifyInstance } from "fastify"

// Fake DB for testing purposes
const automationsDB = {
  1: {
    id: 1,
    name: "Automation 1",
  },
}

const list = async (request: any, reply: any) => {
  return reply.status(200).send({ status: "ok", data: automationsDB, msg: "", errorMsg: "" })
}

const run = async (request: any, reply: any) => {
  return reply.status(200).send({ status: "ok", data: {}, msg: "", errorMsg: "" })
}

export default (app: FastifyInstance, _, done) => {
  app.get("/", list)
  app.post("/run/:id", run)
  done()
}
