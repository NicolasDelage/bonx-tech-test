import { fastify } from "fastify"
import fastifyCors from "@fastify/cors"
import { logger } from "./logger.js"

import automationsPlugin from "./routes/automations.js"

export default function build(options = {}) {
  options = {
    logger,
    ...options,
  }

  const server = fastify(options)

  server.setErrorHandler(async (error, request, reply) => {
    // Logging locally
    logger.error(error)

    reply.status(500).send({ errorMsg: "Something went wrong", error })
  })

  // Enable the fastify CORS plugin
  server.register(fastifyCors, {
    origin: "*",
    methods: "GET,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
  })

  server.get("/", async (request: any, reply: any) => {
    reply.send({ app: "backend", date: new Date() })
  })

  // Routes
  server.register(automationsPlugin, { prefix: "/automations" })

  return server
}
