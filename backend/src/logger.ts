import pino from "pino"

export const logger = pino({
  transport: {
    target: "pino-pretty",
    options: {
      destination: 1,
      colorize: true,
      translateTime: "HH:MM:ss.l",
      ignore: "pid,hostname",
    },
  },
})
