import build from "./app.js"

const server = build()

const PORT = Number(process.env.PORT) || 8080
server.listen({ port: PORT, host: "0.0.0.0" }, (err: any, address: any) => {
  if (err) {
    console.error(err)
    throw err
  }
  server.log.info(`Server listening at ${address}`)
})
