import { defineStore } from "pinia"

export interface Automation {
  id: string
  name: string
}

export type Automations = Record<string, Automation>

const apiBaseURI = import.meta.env.VITE_BACKEND_URL_GITPOD ?? "http://localhost:8080"

export const automationsStore = defineStore("automations", () => {
  const automations = ref<Automations>({})

  /**
   * Load automations from the backend
   */
  async function loadAutomations() {
    fetch(`${apiBaseURI}/automations`, {
      method: "GET",
      cache: "no-cache",
    })
      .then(response => response.json())
      .then(json => automations.value = json.data)
      .catch(error => console.error(error))
  }

  loadAutomations()

  return { automations, loadAutomations }
})
