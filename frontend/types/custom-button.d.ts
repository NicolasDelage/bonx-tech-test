declare module "~/components/CustomButton.vue" {
  import { DefineComponent } from "vue";
  const component: DefineComponent;
  export default component;
}
